## Solution for a test task

This project contains a solution for three test tasks

1. Algorithms
2. General ML
3. OOP

### Installation

1. Make sure you are using clean and separate from other projects venv
2. Go to the root folder of this project
3. In console, run: 

   
    pip install -r requirements.txt

### Project Structure

- counting_islands.py (solution for the 1st task)
- general_ml/* (solution for the 2nd task)
- oop/* (solution for the 3rd task)
- data/* (contains datasets related to this task)
- requirements.txt (file to reproduce Python's environment)
