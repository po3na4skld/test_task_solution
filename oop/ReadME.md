## OOP test task results

This sub-folder has the next structure:
 - digit_classification.py (python file with digit classification algorithms)
 - digit_classifier.py (python file with the DigitClassifier implementation)
 - random_model.py (python file with small implementation of random prediction model)
 
### How to run:

To run tests for DigitClassification models:
 
    python digit_classification.py


To run tests for DigitClassifier model:

    python digit_classifier.py
